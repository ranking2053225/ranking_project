def read_input_file(file_path: str) -> list:
    rows = []
    with open(file_path, 'r') as file:
        for line in file:
            parts = line.split()
            if parts:
                key = parts[0]
                values = list(map(int, parts[1:]))
                rows.append((key, values))
    return rows

def sort_rows(rows: list) -> list:
    return sorted(rows, key=lambda item: item[1][0])

def format_output(sorted_rows: list) -> str:
   

    groups = []
    current_group = [sorted_rows[0][0]]
    current_marks = sorted_rows[0][1]

    for i in range(1, len(sorted_rows)):
        row_marks = sorted_rows[i][1]
        if row_marks == current_marks:
            current_group.append(sorted_rows[i][0])
        else:
            groups.append(" < ".join(current_group))
            current_group = [sorted_rows[i][0]]
            current_marks = row_marks
   
    groups.append(" < ".join(current_group))

    return "\n".join(groups)

def main():
    input_file = 'input.txt'
    rows = read_input_file(input_file)
    sorted_rows = sort_rows(rows)
    output = format_output(sorted_rows)
    print(output)
